// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDtt4cKdkwKur1Jq7vUCpIrKDMSTl7HxyI",
  authDomain: "xibit-ae9c2.firebaseapp.com",
  databaseURL: "https://xibit-ae9c2.firebaseio.com",
  projectId: "xibit-ae9c2",
  storageBucket: "xibit-ae9c2.appspot.com",
  messagingSenderId: "801060355022",
  appId: "1:801060355022:web:593da03adc5c8ba937499f",
  measurementId: "G-67ESQN6768"
};

// Export firebase instance
export const firebase = initializeApp(firebaseConfig)