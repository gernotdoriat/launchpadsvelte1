import { writable } from "svelte/store"

let urlDirectorApi = "https://api.inlume.io"
//let urlDirectorApi = 'http://localhost:3000'

function createLaunchDataStore() {
  const { subscribe, set } = writable({}, start)

  function start() {
    console.log("START LaunchDataStore")
    return stop
  }
  function stop() {
    console.log("STOP LaunchDataStore")
    return
  }

  return {
    subscribe,
    init: async () => {
      let fetched = await fetch(`${urlDirectorApi}/firestore/launchnames`)
      if (fetched.ok) {
        let data = await fetched.json()
        set(data)
        return data
      }
      throw Error(fetched.statusText)
    },
  }
}

export const LaunchDataStore = createLaunchDataStore()
