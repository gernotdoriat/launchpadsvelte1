import { browser } from "$app/env"
import { writable } from "svelte/store"
import {
  getAuth,
  onAuthStateChanged,
  onIdTokenChanged,
  signInWithEmailAndPassword,
  signOut as _signOut,
  verifyPasswordResetCode as _verifyPasswordResetCode,
  confirmPasswordReset as _confirmPasswordReset,
  sendPasswordResetEmail as _sendPasswordResetEmail,
} from "firebase/auth"
import { firebase } from "../services/firebase"
import { async } from "@firebase/util"

const states = {
  // Final states
  signedIn: "signedIn",
  signedOut: "signedOut",
  // Transitional states
  authenticating: "authenticating",
  loading: "loading",
  signingIn: "signingIn",
  signingOut: "signingOut",
  fatalError: "fatalError",
}

const errors = {
  authenticating: "failed authenticating",
  loading: "failed loading",
  signingIn: "failed signingIn",
  signingOut: "failed signingOut",
}

const createAuthStore = () => {
  let storeValue = {
    user: null,
    state: "idle",
    error: null,
  }

  const { subscribe, set } = writable({ storeValue })

  async function loadingUser(user) {
    set(
      (storeValue = {
        ...storeValue,
        state: states.loading,
      })
    )
    if (user) {
      return {
        displayName: user.displayName,
        email: user.email,
        photoURL: user.photoURL,
        emailVerified: user.emailVerified,
      }
    } else {
      set(
        (storeValue = {
          state: states.signingOut,
          error: {
            atStage: storeValue.state,
            type: errors.loading,
            message: "loadingUser(user): user can not be empty.",
          },
        })
      )

      await signOut()
    }
  }

  // Function watches and returns the auth state
  function checkAuth() {
    // set state
    set(
      (storeValue = {
        ...storeValue,
        state: "authenticating",
      })
    )

    // init auth with imported firebase config
    const auth = getAuth(firebase)

    // Use instead of onAuthStateChanged to ensure statefulness if user state remains the same
    onIdTokenChanged(auth, async (user, error) => {
      if (user) {
        // loadUser if not populated yet
        // Prevents this from running on every
        let userData = !storeValue.user ? await loadingUser(user) : storeValue.user

        // TODO: Cookie Madness
        fetch("/login.json")
          .then((response) => {
            if (response.ok) console.log("Cookie set")
          })
          .then(() => {
            // setAuth done
            set(
              (storeValue = {
                user: userData,
                state: states.signedIn,
                error: null,
              })
            )
          })
      } else {
        // TODO: Remove timeout
        // This timeout is needed because auto-subscriptions will trigger "signedOut" before signingOut can be acted upon (see dashboard/__layout)
        setTimeout(() => {
          set(
            (storeValue = {
              user: null,
              state: states.signedOut,
              error: null,
            })
          )
        }, 200)
      }

      if (error) {
        set(
          (storeValue = {
            user: null,
            state: states.signedOut,
            error: {
              atStage: storeValue.state,
              type: error.code,
              message: error.message,
            },
          })
        )
      }
    })
  }

  if (browser) {
    checkAuth()
  }

  function signIn(email, password) {
    const auth = getAuth(firebase)

    // Set state
    // Clear previous errors
    set(
      (storeValue = {
        state: states.signingIn,
        error: null,
      })
    )
    signInWithEmailAndPassword(auth, email, password)
      .then(() => {
        // State transition handled by onIdTokenChanged()
      })
      .catch((error) => {
        set(
          (storeValue = {
            state: states.signedOut,
            error: {
              atStage: storeValue.state,
              type: error.code,
              message: error.message,
            },
          })
        )
      })
  }

  async function signOut() {
    // Signing out
    set(
      (storeValue = {
        ...storeValue,
        state: states.signingOut,
      })
    )

    const auth = getAuth(firebase)

    try {
      // State transition handled by onIdTokenChanged()
      await _signOut(auth)
    } catch (error) {
      // TODO
      // Handle an error occuring during signOut (eg. Firebase Service down)
      // Manually delete firebase session

      set(
        (storeValue = {
          user: null,
          state: states.fatalError,
          error: {
            atStage: storeValue.state,
            type: error.code,
            message: error.message,
          },
        })
      )
    }
  }

  async function verifyPasswordResetCode(actionCode) {
    return _verifyPasswordResetCode(getAuth(firebase), actionCode)
  }

  function confirmPasswordReset(actionCode, newPassword) {
    return _confirmPasswordReset(getAuth(firebase), actionCode, newPassword)
  }

  function sendPasswordResetEmail(emailaddress) {
    return _sendPasswordResetEmail(getAuth(firebase), emailaddress)
  }

  return {
    subscribe,
    signIn,
    signOut,
    sendPasswordResetEmail,
    verifyPasswordResetCode,
    confirmPasswordReset,
  }
}

export const auth = createAuthStore()
