export async function getDeploys(deployType, deployState, branch, searchText, fullSpec) {
  let fetched
  if (branch) {
    fetched = await fetch("https://api.inlume.io/netlify/deploys/id/" + branch)
  } else {
    switch (deployState) {
      case "active":
        fetched = await fetch("https://api.inlume.io/netlify/deploys?archived=false")
        break
      case "archived":
        fetched = await fetch("https://api.inlume.io/netlify/deploys?archived=true")
        break
      default:
        fetched = await fetch("https://api.inlume.io/netlify/deploys")
        break
    }
  }
  let allDeploys = []
  let jsonDeploys = await fetched.json()

  if (branch) {
    jsonDeploys = jsonDeploys.deploys
    for (let index = 0; index < jsonDeploys.length; index++) {
      const item = jsonDeploys[index]
      let createdSecs = item.created_at._seconds
      let createdDate = new Date(0) // The 0 there is the key, which sets the date to the epoch
      createdDate.setUTCSeconds(createdSecs)
      let createdDateString = createdDate.toString()
      createdDateString = createdDateString.substr(0, createdDateString.indexOf("GMT"))
      if (!searchText || searchTextFitsCommits(searchText, item))
        allDeploys.push({
          fullSpec: fullSpec,
          title: item.branch,
          subtitle: createdDateString,
          content: item.title,
          url: item.permalink,
          action: "",
        })
    }
  } else {
    for (let index = 0; index < jsonDeploys.length; index++) {
      const item = jsonDeploys[index]
      let updatedSecs = item.content.updated_at._seconds
      let updatedDate = new Date(0) // The 0 there is the key, which sets the date to the epoch
      updatedDate.setUTCSeconds(updatedSecs)
      let updatedDateString = updatedDate.toString()
      updatedDateString = "UPDATED " + updatedDateString.substr(0, updatedDateString.indexOf("GMT"))
      let createdSecs = item.content.created_at._seconds
      let createdDate = new Date(0) // The 0 there is the key, which sets the date to the epoch
      createdDate.setUTCSeconds(createdSecs)
      let createdDateString = createdDate.toString()
      createdDateString = "CREATED " + createdDateString.substr(0, createdDateString.indexOf("GMT"))
      if (!searchText || searchTextFits(searchText, item))
        allDeploys.push({
          id: item.id,
          fullSpec: fullSpec,
          archived: item.content.archived,
          title: item.content.branch,
          subtitle: updatedDateString + "<br>" + createdDateString,
          content: item.content.title,
          url: item.content.archived ? item.content.links.permalink : item.content.deploy_ssl_url,
          action: item.content.archived ? "activate" : "archive",
        })
    }
  }
  if (branch) return allDeploys

  let envResult = deployType == "nonprod" ? allDeploys.filter((item) => !item.title.startsWith("prod")) : allDeploys.filter((item) => item.title.startsWith(deployType))
  return envResult
}
function searchTextFits(searchText, item) {
  if (item.content.branch.includes(searchText)) return true
  if (item.content.title.includes(searchText)) return true
  return false
}
function searchTextFitsCommits(searchText, item) {
  if (item.branch.includes(searchText)) return true
  if (item.title.includes(searchText)) return true
  return false
}
